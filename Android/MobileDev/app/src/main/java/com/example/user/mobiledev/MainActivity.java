package com.example.user.mobiledev;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void btnRegistration_Click(View v){
        Intent i = new Intent(MainActivity.this, RegistrationActivity.class);
        startActivity(i);
    }

    public void btnLogin_Click(View v){
        Intent i = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(i);
    }
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//
//        FloatingActionButton email = (FloatingActionButton) findViewById(R.id.emailBtn);
//        email.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Intent i = new Intent(Intent.ACTION_SEND);
//                i.setType("message/rfc822");
//                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"sadismek@gmail.com"});
//                i.putExtra(Intent.EXTRA_SUBJECT, "The artwork list is: ");
//                i.putExtra(Intent.EXTRA_TEXT   , artworkList.toString());
//                try {
//                    startActivity(Intent.createChooser(i, "Send e-mail"));
//                } catch (android.content.ActivityNotFoundException ex) {
//                    Toast.makeText(MainActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
//                }
//
//
//            }
//        });
//
//        try {
//            readFile( MainActivity.this);
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, artworkList);
//
//        //ArrayAdapter adapter = new ArrayAdapter<Artwork>(this, android.R.layout.simple_list_item_1, artworkList);
//        final ListView mainListView = (ListView) findViewById(R.id.listView);
//        //adapter.notifyDataSetChanged();
//        mainListView.setAdapter(adapter);
//        mainListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//
//                Artwork a = (Artwork) adapterView.getItemAtPosition(i);
//
//                String[] artw = {a.getName(),a.getArtist(),a.getLikes(),a.getDislikes()};
//
//                Intent intent = new Intent(MainActivity.this, EditActivity.class);
//                intent.putExtra("artwork",artw);
//                intent.putExtra("pos",i);
//                startActivity(intent);
//            }
//        });
//
//        FloatingActionButton refresh = (FloatingActionButton) findViewById(R.id.refreshBtn);
//
//        refresh.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(artworkList.isEmpty()){
//                    Toast.makeText(getBaseContext(), "List is empty", Toast.LENGTH_LONG).show();
//                }else{
//                    ArrayAdapter<Artwork> adapter = new ArrayAdapter<Artwork>(MainActivity.this, android.R.layout.simple_list_item_1, artworkList);
//                    mainListView.setAdapter(adapter);
//                }
//
//            }
//        });
//    }
//
//    public static void createFile(Context ctx) throws IOException, JSONException {
//        JSONArray data = new JSONArray();
//        JSONObject art;
//
//        for (int i = 0; i < artworkList.size(); i++) {
//            art = new JSONObject();
//            art.put("name", artworkList.get(i).getName());
//            art.put("artist", artworkList.get(i).getArtist());
//            art.put("likes", artworkList.get(i).getLikes());
//            art.put("dislikes", artworkList.get(i).getDislikes());
//            data.put(art);
//        }
//        String text = data.toString();
//
//        FileOutputStream fos = ctx.openFileOutput("artworksFile", MODE_PRIVATE);
//        fos.write(text.getBytes());
//        fos.close();
//    }
//
//    public static void readFile( Context ctx) throws IOException, JSONException {
//
//        FileInputStream fis = ctx.openFileInput("artworksFile");
//        BufferedInputStream bis = new BufferedInputStream(fis);
//        StringBuffer b = new StringBuffer();
//        while (bis.available() != 0) {
//            char c = (char) bis.read();
//            b.append(c);
//        }
//        bis.close();
//        fis.close();
//        JSONArray data = new JSONArray(b.toString());
//
//        for (int i = 0; i < data.length(); i++) {
//            String name = data.getJSONObject(i).getString("name");
//            String artist = data.getJSONObject(i).getString("artist");
//            String likes = data.getJSONObject(i).getString("likes");
//            String dislikes = data.getJSONObject(i).getString("dislikes");
//            Artwork artwork = new Artwork(name, artist, likes, dislikes);
//            artworkList.add(artwork);
//        }
//
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        adapter.notifyDataSetChanged();
//        try {
//            createFile( MainActivity.this);
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void onClickAddLayoutButton(View view) {
//        final int result = 1;
//
//        Intent intent = new Intent(this, AddActivity.class);
//        startActivityForResult(intent, result);
//    }
//
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//        super.onActivityResult(requestCode, resultCode, data);
//
//        String[] appSentBack = data.getStringArrayExtra("artwork");
//
//        Artwork artReceived = new Artwork(appSentBack[0], appSentBack[1], appSentBack[2], appSentBack[3]);
//        artworkList.add(artReceived);
//
//        try {
//            createFile( MainActivity.this);
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        ListView lv = (ListView) findViewById(R.id.listView);
//        lv.invalidate();
//
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

}
