package com.example.user.mobiledev;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import static com.example.user.mobiledev.Artwork.artworkList;

public class EditActivity extends AppCompatActivity {

    public int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        Intent intent = getIntent();
        position = intent.getIntExtra("pos", -1);

        String[] art = intent.getStringArrayExtra("artwork");

        EditText nameEdit = (EditText) findViewById(R.id.name_input_edit);
        EditText artistEdit = (EditText) findViewById(R.id.artist_input_edit);
        EditText likesEdit = (EditText) findViewById(R.id.likes_input_edit);
        EditText dislikesEdit = (EditText) findViewById(R.id.dislikes_input_edit);

        nameEdit.append(art[0]);
        artistEdit.append(art[1]);
        likesEdit.append(art[2]);
        dislikesEdit.append(art[3]);
    }

    public void onClickSaveArtworkButton(View view) {
        EditText nameEdit = (EditText) findViewById(R.id.name_input_edit);
        EditText artistEdit = (EditText) findViewById(R.id.artist_input_edit);
        EditText likesEdit = (EditText) findViewById(R.id.likes_input_edit);
        EditText dislikesEdit = (EditText) findViewById(R.id.dislikes_input_edit);

        String nt =String.valueOf(nameEdit.getText());
        String at =String.valueOf(artistEdit.getText());
        String lt =String.valueOf(likesEdit.getText());
        String dt =String.valueOf(dislikesEdit.getText());

        Artwork artw =new Artwork(nt,at,lt,dt);

        artworkList.set(position, artw);
        finish();
    }

    public void onClickDeleteArtworkButton(View view) {

        AlertDialog.Builder bldr = new AlertDialog.Builder(EditActivity.this);
        bldr.setMessage("Are you sure you want to delete this artwork?");
        bldr.setCancelable(true);
        bldr.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        artworkList.remove(position);
                        finish();

                    }
                });
        bldr.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        AlertDialog alrt = bldr.create();
        alrt.show();
    }
}
