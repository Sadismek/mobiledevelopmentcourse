package com.example.user.mobiledev;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NotificationCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import static com.example.user.mobiledev.Artwork.artworkList;

/**
 * Created by User on 1/17/2017.
 */

public class ProfileActivity extends Activity {

    private FirebaseAuth firebaseAuth;
    public ArrayAdapter adapter;
    private DatabaseReference databaseReference;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        firebaseAuth = FirebaseAuth.getInstance();
        if(firebaseAuth.getCurrentUser() == null){
            finish();
            startActivity(new Intent(this,LoginActivity.class));
        }

        databaseReference = FirebaseDatabase.getInstance().getReference();

        Button logout = (Button) findViewById(R.id.btnUserLogout);

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                finish();
            }
        });

        FloatingActionButton email = (FloatingActionButton) findViewById(R.id.emailBtn);
        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"sadismek@gmail.com"});
                i.putExtra(Intent.EXTRA_SUBJECT, "The artwork list is: ");
                i.putExtra(Intent.EXTRA_TEXT   , artworkList.toString());
                try {
                    startActivity(Intent.createChooser(i, "Send e-mail"));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(ProfileActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }


            }
        });

        try {
            readFile(ProfileActivity.this);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, artworkList);

        //ArrayAdapter adapter = new ArrayAdapter<Artwork>(this, android.R.layout.simple_list_item_1, artworkList);
        final ListView mainListView = (ListView) findViewById(R.id.listView);
        //adapter.notifyDataSetChanged();
        mainListView.setAdapter(adapter);
        mainListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Artwork a = (Artwork) adapterView.getItemAtPosition(i);

                String[] artw = {a.getName(),a.getArtist(),a.getLikes(),a.getDislikes()};

                Intent intent = new Intent(ProfileActivity.this, EditActivity.class);
                intent.putExtra("artwork",artw);
                intent.putExtra("pos",i);
                startActivity(intent);
            }
        });

//        FloatingActionButton refresh = (FloatingActionButton) findViewById(R.id.refreshBtn);

//        refresh.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(artworkList.isEmpty()){
//                    Toast.makeText(getBaseContext(), "List is empty", Toast.LENGTH_LONG).show();
//                }else{
//                    ArrayAdapter<Artwork> adapter = new ArrayAdapter<Artwork>(ProfileActivity.this, android.R.layout.simple_list_item_1, artworkList);
//                    mainListView.setAdapter(adapter);
//                }
//
//            }
//        });
    }

    public static void createFile(Context ctx) throws IOException, JSONException {
        JSONArray data = new JSONArray();
        JSONObject art;

        for (int i = 0; i < artworkList.size(); i++) {
            art = new JSONObject();
            art.put("name", artworkList.get(i).getName());
            art.put("artist", artworkList.get(i).getArtist());
            art.put("likes", artworkList.get(i).getLikes());
            art.put("dislikes", artworkList.get(i).getDislikes());
            data.put(art);
        }
        String text = data.toString();

        FileOutputStream fos = ctx.openFileOutput("artworksFile", MODE_PRIVATE);
        fos.write(text.getBytes());
        fos.close();
    }

    public static void readFile( Context ctx) throws IOException, JSONException {

        FileInputStream fis = ctx.openFileInput("artworksFile");
        BufferedInputStream bis = new BufferedInputStream(fis);
        StringBuffer b = new StringBuffer();
        while (bis.available() != 0) {
            char c = (char) bis.read();
            b.append(c);
        }
        bis.close();
        fis.close();
        JSONArray data = new JSONArray(b.toString());

        for (int i = 0; i < data.length(); i++) {
            String name = data.getJSONObject(i).getString("name");
            String artist = data.getJSONObject(i).getString("artist");
            String likes = data.getJSONObject(i).getString("likes");
            String dislikes = data.getJSONObject(i).getString("dislikes");
            Artwork artwork = new Artwork(name, artist, likes, dislikes);
            artworkList.add(artwork);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
        try {
            createFile(ProfileActivity.this);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onClickAddLayoutButton(View view) {
        final int result = 1;

        Intent intent = new Intent(this, AddActivity.class);
        startActivityForResult(intent, result);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        String[] appSentBack = data.getStringArrayExtra("artwork");

        Artwork artReceived = new Artwork(appSentBack[0], appSentBack[1], appSentBack[2], appSentBack[3]);
        artworkList.add(artReceived);

        try {
            createFile(ProfileActivity.this);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ListView lv = (ListView) findViewById(R.id.listView);
        lv.invalidate();

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(ProfileActivity.this)
                        .setSmallIcon(R.drawable.common_google_signin_btn_icon_dark)
                        .setContentTitle("Artwork Gallery")
                        .setContentText("New artwork added!");

        mBuilder.setColor(0xff777777);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(001,mBuilder.build());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
