package com.example.user.mobiledev;

import java.util.ArrayList;

/**
 * Created by User on 1/17/2017.
 */

public class Artwork {

    public static ArrayList<Artwork> artworkList = new ArrayList<>();

    private String name;
    private String artist;
    private String likes;
    private String dislikes;

    public Artwork(String name, String artist, String likes, String dislikes) {
        this.name = name;
        this.artist = artist;
        this.likes = likes;
        this.dislikes = dislikes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getDislikes() {
        return dislikes;
    }

    public void setDislikes(String dislikes) {
        this.dislikes = dislikes;
    }

    @Override
    public String toString() {
        return "Artwork: " + name +
                ", by " + artist +
                ", Likes: " + likes +
                ", Dislikes: " + dislikes;
    }
}
