package com.example.user.mobiledev;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class AddActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
    }

    public void onClickAddArtworkButton(View view) {
        EditText nameText =(EditText)findViewById(R.id.name_input_add);
        EditText artistText =(EditText)findViewById(R.id.artist_input_add);
        EditText likesText =(EditText)findViewById(R.id.likes_input_add);
        EditText dislikesText =(EditText)findViewById(R.id.dislikes_input_add);

        String nt = String.valueOf(nameText.getText());
        String at = String.valueOf(artistText.getText());
        String lt = String.valueOf(likesText.getText());
        String dt = String.valueOf(dislikesText.getText());

        String[] artw = {nt, at, lt, dt};
        Intent goingBackIntent = new Intent(this, MainActivity.class);
        goingBackIntent.putExtra("artwork", artw);
        setResult(RESULT_OK,goingBackIntent);
        finish();

    }
}
