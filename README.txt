The mobile application will be about artists' galleries. There are 2 user roles: artist and regular user.

App features:
- there is a log-in page

- the artist can:	> upload his artwork on the app
					> see a pie chart of his most favourited artwork, and one for his most disliked artwork

- the user can: 	* view a list of all artworks available on the page
 					* like or dislike the artwork
					* search artwork by a certain artist
					* filter artwork by genre (e.g. realism, comics, noir etc.)
					

- the artwork has: 	- a small description
					- date when it was added
					- like count and dislike count